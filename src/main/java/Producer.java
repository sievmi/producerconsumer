import java.util.Scanner;
import java.util.TreeMap;

public class Producer implements Runnable{

    public Producer(TreeMap<Integer, Pair> numbers) {
        this.numbers = numbers;
    }

    public void run() {

        System.out.println("Producer starts work...");

        Converter converter = new Converter();
        while(in.hasNext()) {

            String sNumber = in.nextLine();
            int number = converter.convert(sNumber);

            if(number < 0) {
                System.out.println(sNumber + " is not correct number...");
                continue;
            }

            synchronized (numbers) {
                if(numbers.containsKey(number)) {
                    numbers.get(number).incCnt();
                } else {
                    numbers.put(number, new Pair(sNumber,1));
                }

                System.out.println(sNumber + " = " + number);
                numbers.notifyAll();
            }

        }

    }


    private Scanner in = new Scanner(System.in);
    volatile private TreeMap<Integer, Pair> numbers;
}
