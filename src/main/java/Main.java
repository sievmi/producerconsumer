import java.util.TreeMap;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        TreeMap<Integer, Pair> numbers = new TreeMap<Integer, Pair>();

        Producer producer = new Producer(numbers);
        Thread threadProducer = new Thread(producer);

        Consumer consumer = new Consumer(numbers);
        Thread threadConsumer = new Thread(consumer);

        threadProducer.start();
        threadConsumer.start();

        threadProducer.join();
        System.out.println("Producer finished work");

        threadConsumer.interrupt();
        threadConsumer.join();
        System.out.println("Consumer finished work");
    }
}
