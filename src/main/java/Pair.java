public class Pair {

    public Pair(String number, int cnt) {
        this.number = number;
        this.cnt = cnt;
    }

    private final String number;
    volatile private int cnt;

    public String getNumber() {
        return number;
    }

    public int getCnt() {
        return cnt;
    }

    public void incCnt() {
        this.cnt++;
    }

    public void decCnt() {
        this.cnt--;
    }
}