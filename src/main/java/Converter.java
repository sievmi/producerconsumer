import java.util.ArrayList;
import java.util.StringTokenizer;

public class Converter {

    int convert(String str) {
        ArrayList<String> list = new ArrayList<String>();
        StringTokenizer tokenizer = new StringTokenizer(str, " ");

        while(tokenizer.hasMoreTokens()) {
            list.add(tokenizer.nextToken());
        }

        int num = 0;
        for(int i = 0; i < list.size() / 2; i++) {
            int tempNum = convertPair(list.get(2*i), list.get(2*i+1));
            if(tempNum >= 0)
                num += tempNum;
            else
                return -1;
        }

        if(list.size() % 2 == 1) {
            int tempNum = convertSingle(list.get(list.size() - 1));
            if(tempNum >= 0)
                num += tempNum;
            else
                return -1;
        }

        return num;
    }

    private int convertPair(String first, String second) {
        if(second.equals("hundred")) {
            return 100 * convertSingle(first);
        }

        if(second.equals("thousand")) {
            return 1000 * convertSingle(first);
        }

        if(convertSingle(first) >=0 && convertSingle(second) >= 0)
            return convertSingle(first) + convertSingle(second);
        else
            return -1;
    }

    private int convertSingle(String str) {
        int number;

        number = convertDigit(str);
        if(number!=-1)
            return number;

        number = convertTen(str);
        if(number!=-1)
            return number;

        number = convert11To19(str);
        if(number!=-1)
            return number;

        return -1;
    }

    private int convertDigit(String str) {
        String[] digits = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
        int i;
        for(i = 0; i < digits.length && !str.equals(digits[i]); i++);

        if(i < digits.length)
            return i;
        else
            return -1;
    }

    private int convertTen(String str) {
        String[] ten = {"ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        int i;
        for(i = 0; i < ten.length && !str.equals(ten[i]); i++);
        if(i < ten.length)
            return 10 * (i + 1);
        else
            return -1;
    }

    private int convert11To19(String str) {
        String[] numbers = {"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
                "eighteen", "nineteen" };

        int i;
        for(i=0; i < numbers.length && !str.equals(numbers[i]); i++);
        if(i< numbers.length)
            return 11 + i;
        else
            return -1;
    }
}
