import java.util.TreeMap;

public class Consumer implements Runnable{

    public Consumer(TreeMap<Integer, Pair> numbers) {
        this.numbers = numbers;
    }

    public void run() {

        System.out.println("Consumer starts work...");

        while(flag || numbers.size() > 0) {

            try {
                Thread.sleep(timeOut);
            } catch (InterruptedException e) {
                flag = false;
            }

            if(!flag && numbers.size() == 0)
                break;

            synchronized (numbers) {

                if(numbers.size() == 0) {
                    try {
                        numbers.wait();
                    } catch (InterruptedException e) {
                        flag = false;
                        break;
                    }
                }

                int smallestKey = numbers.firstKey();
                Pair smallestPair = numbers.get(smallestKey);

                System.out.println("Consumer: " + smallestPair.getNumber());

                smallestPair.decCnt();
                if(smallestPair.getCnt() == 0) {
                    numbers.remove(smallestKey);
                }
            }
        }
    }

    final private int timeOut = 5000;
    private boolean flag = true;
    private final TreeMap<Integer, Pair> numbers;

}
