import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static junit.framework.Assert.*;

public class ConverterTest {

    private Converter tester;

    private Method mConvertDigit;
    private Object pConvertDigit;

    private Method mConvertTen;
    private Object pConvertTen;

    private Method mConvert11to19;
    private Object pConvert11to19;

    private Method mConvertSingle;
    private Object pConvertSingle;

    private Method mConvertPair;
    private Object[] pConvertPair;


    private String[] numbers = {"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
            "eighteen", "nineteen" };

    private String[] digits = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

    private String[] ten = {"ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};


    @Before
    public void init() throws Exception {
        tester = new Converter();

        mConvertDigit = tester.getClass().getDeclaredMethod("convertDigit", String.class);
        mConvertDigit.setAccessible(true);

        mConvertTen = tester.getClass().getDeclaredMethod("convertTen", String.class);
        mConvertTen.setAccessible(true);

        mConvert11to19 = tester.getClass().getDeclaredMethod("convert11To19", String.class);
        mConvert11to19.setAccessible(true);

        mConvertSingle = tester.getClass().getDeclaredMethod("convertSingle", String.class);
        mConvertSingle.setAccessible(true);

        mConvertPair = tester.getClass().getDeclaredMethod("convertPair", String.class, String.class);
        pConvertPair = new Object[2];
        mConvertPair.setAccessible(true);
    }

    @Test
    public void testConvert() {
        assertEquals(103, tester.convert("one hundred three"));
        assertEquals(110, tester.convert("one hundred ten"));
        assertEquals(3203, tester.convert("three thousand two hundred three"));
        assertEquals(9999, tester.convert("nine thousand nine hundred ninety nine"));

        for(int i = 0; i < digits.length; i++) {
            for(int j = 0; j <
                    digits.length; j++) {
                String t = i==0 ? "" : digits[i] + " thousand";
                if(j > 0)
                    t+=" " + digits[j] + " hundred";

                for(int k = 0; k <digits.length; k++) {
                    assertEquals(i*1000+j*100+k, tester.convert(t+" "+digits[k]));
                }

                for(int k = 0; k < ten.length; k++) {
                    assertEquals(i*1000+j*100+(k+1)*10, tester.convert(t+" "+ten[k]));
                }

                for(int k = 0; k < numbers.length; k++) {
                    assertEquals(i*1000+j*100+k+11, tester.convert(t+" "+numbers[k]));
                }

                for(int k=1; k < ten.length; k++)
                    for(int l=1; l < digits.length; l++)
                        assertEquals(1000*i+100*j+10*(k+1)+l, tester.convert(t+" "+ten[k]+" "+digits[l]));
            }
        }

    }

    @Test
    public void testConvertPair() throws InvocationTargetException, IllegalAccessException {

        for(int i = 1; i<= 9; i++) {
            pConvertPair[0] = digits[i];
            pConvertPair[1] = "hundred";
            assertEquals(100*i, mConvertPair.invoke(tester, pConvertPair));

            pConvertPair[0] = digits[i];
            pConvertPair[1] = "thousand";
            assertEquals(1000*i, mConvertPair.invoke(tester, pConvertPair));
        }

        for(int i = 1; i<ten.length; i++)
            for(int j = 1; j <= 9; j++) {
                pConvertPair[0] = ten[i];
                pConvertPair[1] = digits[j];
                assertEquals(10*(i+1)+j, mConvertPair.invoke(tester, pConvertPair));
            }

    }

    @Test
    public void testConvertSingle() throws InvocationTargetException, IllegalAccessException {
        for(int i = 0; i < 10; i++) {
            pConvertSingle = digits[i];
            assertEquals(i, mConvertSingle.invoke(tester, pConvertSingle));
        }

        for(int i = 0; i < ten.length; i++) {
            pConvertSingle = ten[i];
            assertEquals(10*(i+1), mConvertSingle.invoke(tester, pConvertSingle));
        }

        for(int i = 0; i < numbers.length; i++) {
            pConvertSingle = numbers[i];
            assertEquals(i+11, mConvertSingle.invoke(tester, pConvertSingle));
        }

        pConvertSingle = "twenty one";
        assertEquals(-1, mConvertSingle.invoke(tester, pConvertSingle));
        pConvertSingle = "no single";
        assertEquals(-1, mConvertSingle.invoke(tester, pConvertSingle));
        pConvertSingle = "abracadabra";
        assertEquals(-1, mConvertSingle.invoke(tester, pConvertSingle));
    }

    @Test
    public void testConvertDigit() throws InvocationTargetException, IllegalAccessException {
        for(int i = 0; i < 10; i++) {
            pConvertDigit = digits[i];
            assertEquals(i, mConvertDigit.invoke(tester, pConvertDigit));
        }

        pConvertDigit = "ten";
        assertEquals(-1, mConvertDigit.invoke(tester, pConvertDigit));
        pConvertDigit = "abc";
        assertEquals(-1, mConvertDigit.invoke(tester, pConvertDigit));
    }

    @Test
    public void testConvert11to19() throws InvocationTargetException, IllegalAccessException {

        for(int i = 0; i < numbers.length; i++) {
            pConvert11to19 = numbers[i];
            assertEquals(i+11, mConvert11to19.invoke(tester, pConvert11to19));
        }

        pConvert11to19 = "four";
        assertEquals(-1, mConvert11to19.invoke(tester, pConvert11to19));
        pConvert11to19 = "bcd";
        assertEquals(-1, mConvert11to19.invoke(tester, pConvert11to19));
    }

    @Test
    public void testConvertTen() throws InvocationTargetException, IllegalAccessException {
        for(int i = 0; i < ten.length; i++) {
            pConvertTen = ten[i];
            assertEquals(10*(i+1), mConvertTen.invoke(tester, pConvertTen));
        }

        pConvertTen = "nineteen";
        assertEquals(-1, mConvertTen.invoke(tester, pConvertTen));
        pConvertTen = "cde";
        assertEquals(-1, mConvertTen.invoke(tester, pConvertTen));
    }

}
